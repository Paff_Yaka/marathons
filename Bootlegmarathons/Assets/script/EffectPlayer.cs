using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class EffectPlayer : MonoBehaviour
{
    [SerializeField] AudioSource button;
    [SerializeField] AudioSource process;
    [SerializeField] AudioSource warp;
    [SerializeField] AudioSource jet;
    [SerializeField] AudioSource fuel;
    [SerializeField] AudioSource win;
    [SerializeField] AudioSource lose;


    public void playbut()
    {
        button.Play();
    }

    public void playprocess()
    {
        process.Play();
    }

    public void playwarp()
    {
        warp.Play();
    }

    public void playjet()
    {
        jet.Play();
    }

    public void playfuel()
    {
        fuel.Play();
    }

    public void playwin()
    {
        win.Play();
    }

    public void playlose()
    {
        lose.Play();
    }
}
