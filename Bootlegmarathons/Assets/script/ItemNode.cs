using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemNode : MonoBehaviour
{

    [SerializeField] Itemholder itmget;
    [SerializeField] bool firstnode = false;
    [SerializeField] GameObject Itemprf;
    [SerializeField] int _OnholdItem;
    private int _index;

    public int index => _index;
    public int OnholdItem => _OnholdItem;


    private void Awake()
    {
        OnstartItemRandomizer();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnstartItemRandomizer()
    {
        if (!firstnode)
        {
            _index = Random.Range(0, itmget.myItemList.GetItem.Length);
        }
        else
        {
            _index = Random.Range(0, itmget.myItemList.GetItem.Length - 1);
        }
        
        //_index = 0;
        _OnholdItem = itmget.myItemList.GetItem[_index]._itemNumber;
        Itemprf = Instantiate(itmget.myItemList.GetItem[_index]._ItemPrf, this.transform);
    }

    public void Itemgathered()
    {
        _OnholdItem = 0;
        DestroyImmediate(Itemprf);
    }
}
