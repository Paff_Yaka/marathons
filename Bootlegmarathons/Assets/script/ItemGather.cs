using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGather : MonoBehaviour
{
    [SerializeField] Nodeholder nodeholder;
    [SerializeField] Itemholder itmget;
    [SerializeField] ActionController AC;

    [SerializeField] GameObject Itembag;
    [SerializeField] int ItemcodeInbag;
    [SerializeField] Transform bagpos;

    [SerializeField] CharacterBase charbase;

    int nodenum;

    int Itemindex;


    public void Gather()
    {
        nodenum = nodeholder.curnode;

        if (nodeholder.node[nodenum].GetComponent<Node>().itemnode.GetComponent<ItemNode>().OnholdItem != 0)
        {
            
            Itemindex = nodeholder.node[nodenum].GetComponent<Node>().itemnode.GetComponent<ItemNode>().index;
            //Debug.Log("ItmIND" + Itemindex);
            if (Itembag == null)
            {
                
                Itembag = Instantiate(itmget.myItemList.GetItem[Itemindex]._ItemPrf, bagpos);
                ItemcodeInbag = itmget.myItemList.GetItem[Itemindex]._itemNumber;
                nodeholder.node[nodenum].GetComponent<Node>().itemnode.GetComponent<ItemNode>().Itemgathered();
            }
            
        }
    }

    public void useItem()
    {
        AC.Itemused(ItemcodeInbag,charbase.getcharacterType());
        DestroyImmediate(Itembag);
        ItemcodeInbag = 0;
    }
}
