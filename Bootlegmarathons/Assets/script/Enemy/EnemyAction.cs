using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAction : MonoBehaviour
{
    [SerializeField] public int enemActioncode;

    [SerializeField] ActionController AC;

    [SerializeField] ActionHolder AH;

    [SerializeField] ItemGather itemuse;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActRand()
    {
        int i = Random.Range(0, AH.Actioncode.Length);
        enemActioncode = AH.Actioncode[i];
        if(enemActioncode == 4)
        {
            ActRand();
        }
        else
        {
            AC.ActionCompare();
            AH.holdAction();

            int itemrand = Random.Range(0, 100);
            if (itemrand >= 50)
            {
                //itemuse.useItem(Random.Range(0, itemuse._ItemcodeInbag.Length));
                itemuse.useItem();
            }
        }
        
    }
}
