using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EItemGather : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Nodeholder nodeholder;
    [SerializeField] Itemholder itmget;
    [SerializeField] ActionController AC;

    [SerializeField] GameObject[] Itembag;
    [SerializeField] int[] ItemcodeInbag;
    public int[] _ItemcodeInbag => ItemcodeInbag;
    [SerializeField] Transform[] bagpos;

    int nodenum;

    int Itemindex;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Gather()
    {
        nodenum = nodeholder.curnode;

        if (nodeholder.node[nodenum].GetComponent<Node>().itemnode.GetComponent<ItemNode>().OnholdItem != 0)
        {


            Itemindex = nodeholder.node[nodenum].GetComponent<Node>().itemnode.GetComponent<ItemNode>().index;


            for (int i = 0; i < Itembag.Length; i++)
            {
                if (Itembag[i] == null)
                {
                    Itembag[i] = Instantiate(itmget.myItemList.GetItem[Itemindex]._ItemPrf, bagpos[i]);
                    ItemcodeInbag[i] = itmget.myItemList.GetItem[Itemindex]._itemNumber;
                }
            }

            nodeholder.node[nodenum].GetComponent<Node>().itemnode.GetComponent<ItemNode>().Itemgathered();
        }
    }

    public void useItem(int index)
    {
        AC.Itemused(ItemcodeInbag[index],"E");
        DestroyImmediate(Itembag[index]);
        ItemcodeInbag[index] = 0;
    }
}
