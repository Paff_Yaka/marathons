using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI; 

public class AudSlidert : MonoBehaviour
{

    

    [SerializeField] AudioMixer mix;
    [SerializeField] string volname;

    // Start is called before the first frame update
    public void UpdateVolume(float value)
    {
        mix.SetFloat(volname, Mathf.Log(value) * 20f);
    }
}
