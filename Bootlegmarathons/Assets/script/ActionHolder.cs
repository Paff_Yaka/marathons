using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionHolder : MonoBehaviour
{

    [SerializeField] int[] _Actioncode;
    [SerializeField] Text[] ActionText;

    public int[] Actioncode => _Actioncode;

    [SerializeField] PlayerActionSelector playeract;

    // Start is called before the first frame update
    void Start()
    {
        holdAction();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void holdAction()
    {
        for (int i = 0; i< _Actioncode.Length; i++)
        {
                for (int k = _Actioncode.Length - 1; k >= 0; k--)
                {
                    if(k != 0)
                    {
                        _Actioncode[k] = _Actioncode[k - 1];
                    }
                    
                }

                _Actioncode[i] = playeract.playeractioncode;
                break;
        }

        for (int a = 0; a < ActionText.Length; a++)
        {
            switch (_Actioncode[a])
            {
                case 1:
                    ActionText[a].text = "M";
                    break;
                case 2:
                    ActionText[a].text = "R";
                    break;
                case 3:
                    ActionText[a].text = "G";
                    break;
                case 4:
                    ActionText[a].text = "";
                    break;
                default:
                    ActionText[a].text = "";
                    break;
            }
        }
    }
}
