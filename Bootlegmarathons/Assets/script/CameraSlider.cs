using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSlider : MonoBehaviour
{
    [SerializeField] float panspeed = 20f;
    [SerializeField] float panBorderTHICCness = 10f;

    [SerializeField] Vector2 panlimit;

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        if(Input.mousePosition.x >= Screen.width - panBorderTHICCness || Input.GetKey("d"))
        {
            pos.x += panspeed * Time.deltaTime;
        }
        else if (Input.mousePosition.x <= panBorderTHICCness || Input.GetKey("a"))
        {
            pos.x -= panspeed * Time.deltaTime;
        }

        pos.x = Mathf.Clamp(pos.x, -panlimit.x, panlimit.x);

        transform.position = pos;
    }
}
