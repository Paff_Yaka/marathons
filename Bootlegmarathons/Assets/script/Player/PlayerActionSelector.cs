using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionSelector : MonoBehaviour
{

    [SerializeField] public int playeractioncode;
    [SerializeField] ActionHolder AH;
    [SerializeField] ActionController AC;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move()
    {
        playeractioncode = 1;
        
        AC.playeractselected();
    }

    public void Rest()
    {
        playeractioncode = 2;
        
        AC.playeractselected();
    }

    public void Gather()
    {
        playeractioncode = 3;
        
        AC.playeractselected();
    }


}
