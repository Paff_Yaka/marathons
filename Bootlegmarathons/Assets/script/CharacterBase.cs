using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CharacterBase : MonoBehaviour
{
    [SerializeField] int currentnode = 0;
    [SerializeField] Nodeholder nodeholder;
    [SerializeField] Posholder Posholder;
    public int fuel = 3;
    [SerializeField] Text FUELTXT;
    [SerializeField] int winnode;
    [SerializeField] public ItemGather itmgat;
    public int _currentnode => currentnode;

    public int movelenght;
    [SerializeField] int basemovelenght;
    [SerializeField] ActionController AC;



    void Start()
    {
        this.transform.position = Posholder.pos[currentnode].transform.position;
        nodeholder.curnode = currentnode;

        movelenght = basemovelenght;
    }

    private void Update()
    {
        if(fuel > 3)
        {
            fuel = 3;
        }

        if(fuel != 0)
        {
            FUELTXT.text = "Fuel: " + fuel;
        }
        else
        {
            FUELTXT.text = "Fuel: EMPTY";
        }
    }

    public void MoveByNode()
    {
        if(fuel > 0)
        {
            fuel -= 1;
            FUELTXT.text = "Fuel: " + fuel;
            currentnode += movelenght;
            if (currentnode >= nodeholder.node.Length)
            {
                currentnode = nodeholder.node.Length - 1;
                nodeholder.curnode = currentnode;
                this.transform.position = Posholder.pos[currentnode].transform.position;
                movelenght = basemovelenght;
            }
            else
            {
                nodeholder.curnode = currentnode;
                this.transform.position = Posholder.pos[currentnode].transform.position;
                movelenght = basemovelenght;
            }
        }



        if (currentnode == winnode)
        {
            AC.win(getcharacterType());
        }
        
    }

    public void MoveBackByNode()
    {
        
            
            
            currentnode -= movelenght;
            if (currentnode >= nodeholder.node.Length)
            {
                currentnode = nodeholder.node.Length - 1;
                nodeholder.curnode = currentnode;
                this.transform.position = Posholder.pos[currentnode].transform.position;
                movelenght = basemovelenght;
            }
            else
            {
                nodeholder.curnode = currentnode;
                this.transform.position = Posholder.pos[currentnode].transform.position;
                movelenght = basemovelenght;
            }
       
    }

    public abstract string getcharacterType();
}
