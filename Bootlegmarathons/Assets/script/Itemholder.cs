using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Itemholder : MonoBehaviour
{

    [System.Serializable]
    public class item
    {

        [SerializeField] string _ItemName;
        [SerializeField] int itemNumber;
        public int _itemNumber => itemNumber;

        [SerializeField] GameObject ItemPrf;

        public GameObject _ItemPrf => ItemPrf;


    }

    [System.Serializable] public class myItem
    {
        public item[] GetItem;
    }

    public myItem myItemList = new myItem();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
