using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionController : MonoBehaviour
{
    [SerializeField] PlayerActionSelector playeract;
    [SerializeField] EnemyAction enemyact;
    [SerializeField] ActionHolder AH;

    [SerializeField] PlayerBase Pbase;
    [SerializeField] EnemyBase Ebase;

    [SerializeField] Text Ptext;
    [SerializeField] Text Etext;

    [SerializeField] Text VS;
    [SerializeField] Text TURN;

    [SerializeField] Button[] PBUT;

    [SerializeField] Nodeholder Enodeholder;
    [SerializeField] Nodeholder Pnodeholder;

    [SerializeField] EffectPlayer EF;

    [SerializeField] GameObject WinCanvas;
    [SerializeField] Text EndText;

    bool equal = false;
    

    [SerializeField] int _turn = -1;

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_turn > 0)
        {
            TURN.text = "TURN: " + _turn;
        }
        else
        {
            TURN.text = "FREETURN";
        }
    }

    public void playeractselected()
    {
        if (_turn > 0)
        {
            enemyact.ActRand();
        }
        else
        {
            ActionCompare();
            AH.holdAction();
        }

            
    }

    public void ActionCompare()
    {
        if(_turn <= 0)
        {
            switch (playeract.playeractioncode)
            {
                case 3:
                    Pbase.itmgat.Gather();
                    _turn += 1;
                    break;
                case 2:
                    //rest
                    Pbase.fuel += 1;
                    _turn += 1;
                    break;
                case 1:
                    Pbase.MoveByNode();
                    _turn += 1;
                    break;
            }
        }
        else
        {
            _turn += 1;

            StartCoroutine(StartCompare());
        }
        

    }

    private IEnumerator StartCompare()
    {
        equal = true;

        for (int i = 0; i < PBUT.Length; i++)
        {
            PBUT[i].interactable = false;
        }
        EF.playprocess();
        switch (playeract.playeractioncode)
        {
            case 1:
                Ptext.text = "PLAYER: MOVE";
                break;
            case 2:
                Ptext.text = "PLAYER: REFIL";
                break;
            case 3:
                Ptext.text = "PLAYER: GATHER";
                break;
        }

        yield return new WaitForSeconds(1f);
        VS.text = "VS";
        yield return new WaitForSeconds(1f);

        switch (enemyact.enemActioncode)
        {
            case 1:
                Etext.text = "ENEMY: MOVE";
                break;
            case 2:
                Etext.text = "ENEMY: REFIL";
                break;
            case 3:
                Etext.text = "ENEMY: GATHER";
                break;
        }

        ActionEva(playeract.playeractioncode, enemyact.enemActioncode, Pbase, Ebase);
        ActionEva(enemyact.enemActioncode, playeract.playeractioncode, Ebase, Pbase);

        yield return new WaitForSeconds(1f);
        clearTXT();
    }

    void clearTXT()
    {
        Etext.text = "";
        Ptext.text = "";
        VS.text = "";

        for (int i = 0; i < PBUT.Length; i++)
        {
            PBUT[i].interactable = true;
        }
    }

    public void ActionEva(int First, int Second, CharacterBase firstChar, CharacterBase secondChar)
    {
        if (First > Second)
        {
            equal = false;
            switch (First)
            {
                case 2:
                    secondChar.MoveByNode();
                    break;
                case 3:
                    if (Second == 2)
                    {
                        firstChar.itmgat.Gather();
                        secondChar.itmgat.Gather();
                        secondChar.fuel += 1;
                    }
                    else
                    {
                        secondChar.MoveByNode();
                        firstChar.itmgat.Gather();
                        firstChar.MoveByNode();
                        
                    }
                    break;
            }


        }
        else if (First == Second)
        {
            if(equal == true)
            {
                switch (First)
                {
                    case 1:
                        firstChar.MoveByNode();
                        secondChar.MoveByNode();
                        break;
                    case 2:
                        firstChar.fuel += 1;
                        secondChar.fuel += 1;
                        // both stop
                        break;
                    case 3:
                        if (Enodeholder.curnode != (Pnodeholder.curnode - 2))
                        {
                            firstChar.itmgat.Gather();
                            secondChar.itmgat.Gather();
                        }

                        break;
                }

                equal = false;
            }
                
        }
    }

    public void Itemused(int itemnum, string User)
    {
        if(User == "P")
        {
            switch (itemnum)
            {
                case 3:
                    EF.playwarp();
                    Ebase.MoveBackByNode();
                    break;
                case 2:
                    EF.playfuel();
                    Pbase.fuel += 2;
                    break;
                case 1:
                    EF.playjet();
                    Pbase.movelenght += 1;
                    break;
            }
        }
        else
        {
            switch (itemnum)
            {
                case 3:
                    EF.playwarp();
                    Pbase.MoveBackByNode();
                    break;
                case 2:
                    EF.playfuel();
                    Ebase.fuel += 2;
                    break;
                case 1:
                    EF.playjet();
                    Ebase.movelenght += 1;
                    break;
            }
        }
        
    }

    public void win(string user)
    {
        if (user == "P")
        {
            EF.playwin();
            WinCanvas.SetActive(true);
            EndText.text = "WIN!";
        }
        else
        {
            EF.playlose();
            WinCanvas.SetActive(true);
            EndText.text = "LOSE!";
        }
    }

}
