using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class UIMENU : MonoBehaviour
{
    [SerializeField] GameObject pausing;

    [SerializeField] bool ispausing = false;

    [SerializeField] bool mainmenu = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!mainmenu)
        {
            if (!ispausing)
            {

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ispausing = true;
                    pausing.SetActive(true);
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ispausing = false;
                    pausing.SetActive(false);
                }
            }
        }
        
        
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
